﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReadifyService.Business;

namespace ReadifyService.Tests
{
    [TestClass]
    public class TestsShapeTriangle
    {
        [TestMethod]
        public void Test_WhatShapeIsThis_equilateral()
        {
            var shape = Triangle.WhatShapeIsThis(2, 2, 2);
            Assert.AreEqual(shape, TriangleType.Equilateral);
        }

        [TestMethod]
        public void Test_WhatShapeIsThis_isosceles()
        {
            var shape = Triangle.WhatShapeIsThis(2, 2, 3);
            Assert.AreEqual(shape, TriangleType.Isosceles);
        }

        [TestMethod]
        public void Test_WhatShapeIsThis_scalene()
        {
            var shape = Triangle.WhatShapeIsThis(3, 4, 5);
            Assert.AreEqual(shape, TriangleType.Scalene);
        }

        [TestMethod]
        public void Test_WhatShapeIsThis_error_if_0()
        {
            var shape = Triangle.WhatShapeIsThis(0, 2, 2);
            Assert.AreEqual(shape, TriangleType.Error);
        }

        [TestMethod]
        public void Test_WhatShapeIsThis_error_two_sides_equal_third()
        {
            var shape = Triangle.WhatShapeIsThis(2, 2, 4);
            Assert.AreEqual(shape, TriangleType.Error);
        }

        [TestMethod]
        public void Test_WhatShapeIsThis_error_two_equal_sides_sum_less_than_third()
        {
            var shape = Triangle.WhatShapeIsThis(2, 2, 20);
            Assert.AreEqual(shape, TriangleType.Error);
        }

        [TestMethod]
        public void Test_WhatShapeIsThis_error_two_different_sides_sum_less_than_third()
        {
            var shape = Triangle.WhatShapeIsThis(1, 2, 5);
            Assert.AreEqual(shape, TriangleType.Error);
        }

        [TestMethod]
        public void Test_WhatShapeIsThis_error_negative_integer()
        {
            var shape = Triangle.WhatShapeIsThis(-1, -1, 2);
            Assert.AreEqual(shape, TriangleType.Error);
        }
    }
}
