﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReadifyService.Business;

namespace ReadifyService.Tests
{
    [TestClass]
    public class TestsReverseWords
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_Reverse_null()
        {
            Words.ReverseWords(null);
        }

        [TestMethod]
        public void Test_Reverse_empty()
        {
            var text = Words.ReverseWords(string.Empty);
            Assert.AreEqual(text, string.Empty);
        }

        [TestMethod]
        public void Test_Reverse_one_character()
        {
            var text = Words.ReverseWords("A");
            Assert.AreEqual("A", text);
        }

        [TestMethod]
        public void Test_Reverse_word_tiger()
        {
            var text = Words.ReverseWords("TiGeR");
            Assert.AreEqual("ReGiT", text);
        }

        [TestMethod]
        public void Test_Reverse_2_words()
        {
            var text = Words.ReverseWords("Hungry Tiger");
            Assert.AreEqual("yrgnuH regiT", text);
        }

        [TestMethod]
        public void Test_Reverse_2_words_space_at_end()
        {
            var text = Words.ReverseWords("Hungry Tiger ");
            Assert.AreEqual("yrgnuH regiT ", text);
        }

        [TestMethod]
        public void Test_Reverse_2_words_space_at_beginning()
        {
            var text = Words.ReverseWords(" Hungry Tiger");
            Assert.AreEqual(" yrgnuH regiT", text);
        }

        [TestMethod]
        public void Test_Reverse_2_words_2_spaces_at_beginning()
        {
            var text = Words.ReverseWords("  Hungry Tiger");
            Assert.AreEqual("  yrgnuH regiT", text);
        }

        [TestMethod]
        public void Test_Reverse_3_words()
        {
            var text = Words.ReverseWords("Hungry indian Tiger");
            Assert.AreEqual("yrgnuH naidni regiT", text);
        }

        [TestMethod]
        public void Test_letters_separated_by_two_spaces()
        {
            var text = Words.ReverseWords("  H  E  L  L  O  ");
            Assert.AreEqual("  H  E  L  L  O  ", text);
        }

        [TestMethod]
        public void Test_letters_separated_by_question_marks()
        {
            var text = Words.ReverseWords("H?E?L?L?O");
            Assert.AreEqual("O?L?L?E?H", text);
        }
    }
}
