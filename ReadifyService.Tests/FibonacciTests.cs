﻿using System;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReadifyService.Business;

namespace ReadifyService.Tests
{
    [TestClass]
    public class FibonacciTests
    {
        public void Test_Fibonacci_0()
        {
            var number = Fibonacci.GetFibonacci(0);
            Assert.AreEqual(0, number);
        }

        [TestMethod]
        public void Test_Fibonacci_6()
        {
            var number = Fibonacci.GetFibonacci(6);
            Assert.AreEqual(8, number);
        }

        [TestMethod]
        public void Test_Fibonacci_13()
        {
            var number = Fibonacci.GetFibonacci(13);
            Assert.AreEqual(233, number);
        }

        [TestMethod]
        public void Test_Fibonacci_last_valid_long_92()
        {
            var number = Fibonacci.GetFibonacci(92);
            Assert.AreEqual(7540113804746346429, number);
            
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Test_Fibonacci_argument_exception_93()
        {
            Fibonacci.GetFibonacci(93);
        }
    }
}
