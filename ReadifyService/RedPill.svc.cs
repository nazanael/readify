﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using ReadifyService.Business;

namespace ReadifyService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class RedPill : IRedPill
    {
        private readonly ILogger logger;

        public RedPill() : this(ServiceLocator.Current.GetInstance<ILogger>()) { }

        public RedPill(ILogger _logger)
        {
            logger = _logger;
        }
        public Guid WhatIsYourToken()
        {
            try
            {
                var token = ConfigurationManager.AppSettings["Token"];
                return new Guid(token);
            }
            catch (ConfigurationErrorsException ex)
            {
                logger.LogException(ex);
                return Guid.Empty;
            }
        }

        public long FibonacciNumber(long n)
        {
            try
            {
                return Fibonacci.GetFibonacci(n);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                logger.LogException(ex);
                throw new FaultException<ArgumentOutOfRangeException>(ex);
            }
        }

        public TriangleType WhatShapeIsThis(int a, int b, int c)
        {
            return Triangle.WhatShapeIsThis(a, b, c);
        }

        public string ReverseWords(string s)
        {
            try
            {
                return Words.ReverseWords(s);
            }
            catch(ArgumentNullException ex)
            {
                logger.LogException(ex);
                throw new FaultException<ArgumentNullException>(ex);
            }
        }
    }
}
