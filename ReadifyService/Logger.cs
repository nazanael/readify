﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using log4net;

namespace ReadifyService
{
    public interface ILogger
    {
        void LogException(Exception exception);
        void LogInfo(string message); 
    }

    public class Logger : ILogger
    {

        #region Fields

        private static readonly ILog mLog = LogManager.GetLogger(typeof(Logger));

        #endregion Fields

        #region Constructor

        public Logger()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        #endregion Constructor

        #region Public Methods

        public void LogException(Exception exception)
        {
            if (mLog != null)
                mLog.Error("Exception", exception);
        }

        private void LogInfo(string category, string message)
        {
            if (mLog != null)
                mLog.Info(FormatMessage(category, message));
        }

        public void LogInfo(string message)
        {
            LogInfo("Information", message);
        }
        

        #endregion Public Methods

        #region Private Methods

        private const string MessageFormat = "{0} | {1}";
        private const int MaxCategoryNameLength = 25;

        private static string FormatMessage(string category, string message)
        {
            var output = string.Format(MessageFormat, FormatName(category,
                                                                    MaxCategoryNameLength), message);
            return output;
        }

        private static string FormatName(string name, int minLength)
        {
            var trimName = name != null ? name.Trim() : string.Empty;
            var result = trimName.Length >= minLength ? trimName : trimName.PadRight(minLength);
            return result;
        }

        #endregion Private Methods
    }
}