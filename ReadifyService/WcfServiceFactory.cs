using Microsoft.Practices.Unity;
using Unity.Wcf;

namespace ReadifyService
{
	public class WcfServiceFactory : UnityServiceHostFactory
    {
        protected override void ConfigureContainer(IUnityContainer container)
        {
             container
                .RegisterType<ILogger, Logger>(new HierarchicalLifetimeManager())
                .RegisterType<IRedPill, RedPill>();
        }
    }    
}