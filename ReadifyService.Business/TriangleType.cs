﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadifyService.Business
{
    public enum TriangleType : int
    {

        Error = 0,
        Equilateral = 1,
        Isosceles = 2,
        Scalene = 3,
    }
}
