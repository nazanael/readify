﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadifyService.Business
{
    public class Words
    {
        public static string ReverseWords(string s)
        {
            if (s == null)
                throw new ArgumentNullException("s");
            var words = s.Split(' ');
            var array = words.Select(x => new String(x.Reverse().ToArray()));
            return String.Join(" ", array);
        }
    }
}
