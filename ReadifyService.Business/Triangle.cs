﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadifyService.Business
{
    public static class Triangle
    {
        public static TriangleType WhatShapeIsThis(int a, int b, int c)
        {
            if (a <= 0 || b <= 0 || c <= 0)
                return TriangleType.Error;
            if ((a == b) && (b== c))
                return TriangleType.Equilateral;
            if ((a+b <= c) || (b+c <= a) || (c+a <= b))
                return TriangleType.Error;
            if ((a==b) || (b==c) || (a==c))
                return TriangleType.Isosceles;
            return TriangleType.Scalene;
        }
    }
}
